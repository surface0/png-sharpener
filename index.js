const imagemin = require('imagemin');
const pngquant = require("imagemin-pngquant");
const prompt = require('prompt');
const glob = require('glob');
const path = require('path');

const SRC_DIR = './src';
const DST_DIR = './dst';

const COMPRESS_OPTION_DEFAULT = {
	quality: '40-65',
	speed: 3,
	floyd: 0.5,
};

const compress = (options) => {
    options = {...COMPRESS_OPTION_DEFAULT, ...options};
    options.speed = parseInt(options.speed);
    options.floyd = Number(options.floyd);
    
    console.log('pngquant options:', options);

    const imageminOpts = {
        plugins: [pngquant(options)],
    };

    const files = glob.sync(path.join(SRC_DIR, '**/*.png'));

    return Promise.all(files.map((src) => {
        const dst = path.join(DST_DIR, path.relative(SRC_DIR, src));
        console.log('%s -> %s', src, dst);
        
        return imagemin([src], path.dirname(dst), imageminOpts);
    }));
};

const showParamsPrompt = () => {
    return new Promise((resolve) => {
        prompt.start();
        prompt.get([
                {name: 'quality', type: 'string', description: '画質範囲{0-100}(min-max)[40-65]'},
                {name: 'speed', type: 'integer', description: '速度{1-10}（1:遅い/高画質～10:速い/低画質）[3]'},
                {name: 'floyd', type: 'number', description: 'ディザリング強度{0～1.0}[0.5]'}
            ],
            (err, result) => resolve(result)
        );
    });
};

async function main() {
    let options;

    switch (process.argv[2]) {
        case 'manual':
            console.log('# manual setting');
            options = await showParamsPrompt();
            break;

        case 'size':
            console.log('# size priority mode');
            options = {
                speed: 1,
                floyd: 1,
                quality: "0-10",
            };
            break;

        case 'quality':
            console.log('# quality priority mode');
            options = {
                speed: 1,
                floyd: 0.8,
                quality: "60-100",
            };
            break;
        
        case 'default':
        default:
            console.log('# balance mode');
            options = {
                speed: 3,
                quality: "40-65",
            };
            break;
    }

    compress(options)
    .then(
        () => console.log('completed.'),
        (reason) => console.log(reason)
    );
}

main();